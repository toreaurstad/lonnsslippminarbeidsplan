﻿namespace MinArbeidsplan
{
    public static class Constants
    {

        public const string MinArbeidsplanUsername = "MinArbeidsplanUsername";

        public const string MinArbeidsplanPassword = "MinArbeidsplanPassword";

        public const string MinArbeidsplanBaseUri = "MinArbeidsplanBaseUri";

        public const string LaunchExplorerAfterRetrieval = "LaunchExplorerAfterRetrieval";

        public const string MinArbeidsplanEmail = "MinArbeidsplanEmail";

        public const string MinArbeidsplanEnvironmentId = "MinArbeidsplanEnvironmentId";


    }
}
