﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MinArbeidsplan
{
    class Program
    {
        // ReSharper disable once UnusedParameter.Local
        static void  Main(string[] args)
        {

            Console.WriteLine("Starting MinArbeidsplan console app..");


            var httpClient = new HttpClient();

            GetTietoAccessToken(httpClient).Wait();

            RequestPaycheck(httpClient).Wait();

            Thread.Sleep(2000);

        }    

        private static string GetAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private static string GetUrlInMinArbeidsplan(string relativePath)
        {
            return Path.Combine(ConfigurationManager.AppSettings[Constants.MinArbeidsplanBaseUri], relativePath);
        }

        private static async Task GetTietoAccessToken(HttpClient httpClient)
        {
            Dictionary<string, string> queryStringParameters = new Dictionary<string, string>
            {
                {"username", GetAppSetting(Constants.MinArbeidsplanUsername)},
                {"password", GetAppSetting(Constants.MinArbeidsplanPassword)},
                {"environmentId", GetAppSetting(Constants.MinArbeidsplanEnvironmentId)},
                {"client_id", "MAP"},
                {"timeZoneId", "Europe/Oslo"},
                {"dst", "true"},
                {"grant_type", "password"}

            };


            Console.WriteLine("Trying to retrieve token for MinArbeidsplan...");


            HttpResponseMessage reply = await httpClient.PostAsync(GetUrlInMinArbeidsplan("api/token"),
                new FormUrlEncodedContent(queryStringParameters));
            string tokenReply = await reply.Content.ReadAsStringAsync();
            TietoToken token = JsonConvert.DeserializeObject<TietoToken>(tokenReply);
            if (token == null || string.IsNullOrEmpty(token.Access_Token))
                throw new SecurityException("Requesting token from Min Arbeidsplan failed! Check your App settings for correct credentials!");
            Console.WriteLine("Token retrieved. Setting up some security setup of HttpClient...");


            ConfigureSecurity(httpClient, token);

        }

        private static void ConfigureSecurity(HttpClient httpClient, TietoToken token)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            httpClient.DefaultRequestHeaders.Add("Referer", GetUrlInMinArbeidsplan("mydata/reports/sickcertificate"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Access_Token);
        }

        private static async Task RequestPaycheck(HttpClient httpClient)
        {
            try
            {
                HttpResponseMessage message =
                    await httpClient.GetAsync(GetUrlInMinArbeidsplan("api/wagedocument/paycheck"));
                string messageContent = await message.Content.ReadAsStringAsync();
                //Console.WriteLine(messageContent);
                List<Paycheck> paychecks = JsonConvert.DeserializeObject<List<Paycheck>>(messageContent);

                bool isNewPaycheckFound = false;
                foreach (var paycheck in paychecks)
                {
                    string payCheckPdfFilepath = Path.GetTempPath() + paycheck.Description + ".pdf";
                    if (File.Exists(payCheckPdfFilepath))
                    {
                        continue; //file saved already to disk - continue looking for more paychecks!
                    }
                    isNewPaycheckFound = true;

                    Console.WriteLine("Found a new paycheck! The paycheck's description is: " + paycheck.Description);
                    Console.WriteLine("Trying to retrieve this paycheck {0} PDF! Downloading...", paycheck.Description);
                    string paycheckUrl = Path.Combine(GetUrlInMinArbeidsplan(@"api/wagedocument/paycheck/" + paycheck.Key1));
                    byte[] paycheckPdf = await httpClient.GetByteArrayAsync(paycheckUrl);

                    Console.WriteLine("Paycheck downloaded! The paycheck {0} will be saved into the following location{1}", paycheck.Description, payCheckPdfFilepath);

                    File.WriteAllBytes(payCheckPdfFilepath, paycheckPdf);

                    Console.WriteLine("Paycheck saved to disk!");

                    Console.WriteLine("Sending new paycheck to email address: {0}", GetAppSetting(Constants.MinArbeidsplanEmail));

                    SendPaycheckWithEmail(payCheckPdfFilepath,  paycheck);

                    Console.WriteLine("Email with attached paycheck {0} sent!", paycheck.Description);
                }

                if (!isNewPaycheckFound)
                    Console.WriteLine("Sorry! No new paychecks found! Try later.");
                else
                {
                    bool launchExplorer = bool.Parse(GetAppSetting(Constants.LaunchExplorerAfterRetrieval));
                    if (launchExplorer)
                        Process.Start("explorer.exe", Path.Combine(Path.GetTempPath()));
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        private static void SendPaycheckWithEmail(string payCheckPdfFilepath, Paycheck paycheck)
        {
            using (SmtpClient smtpServer = new SmtpClient("smtp-relay.helsemn.no"))
            {
                using (var message = new MailMessage())
                {
                    message.From = new MailAddress("no-reply@hemit.no");
                    message.To.Add(GetAppSetting(Constants.MinArbeidsplanEmail));
                    message.Subject = "Lønnsslipp HEMIT - beskrivelse: " + paycheck.Description;
                    message.Body = "Lønnsslipp vedlagt som PDF. Paycheck data from Tieto MinArbeidsplan: " +
                                   Environment.NewLine +
                                   "Paycheck Key1: " + paycheck.Key1 + Environment.NewLine +
                                   "Paycheck CompanyId: " + paycheck.CompanyId + Environment.NewLine +
                                   "Paycheck EmploymentNo: " + paycheck.EmploymentNo + Environment.NewLine +
                                   "Paycheck Periode:" + paycheck.Periode + Environment.NewLine;
    
                    message.Attachments.Add(new Attachment(payCheckPdfFilepath));

                    //smtpServer.UseDefaultCredentials = false; 
                    //smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //smtpServer.Port = 587;
                    //smtpServer.Credentials = new System.Net.NetworkCredential(..", "..", "..");
                    //smtpServer.EnableSsl = true;

                    smtpServer.Send(message);
                }
            }


        }

    }
}
