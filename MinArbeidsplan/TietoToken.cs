﻿using System;

namespace MinArbeidsplan
{
    public class TietoToken
    {
        public string Access_Token { get; set; }

        public string Token_Type { get; set; }

        public string Expires_In { get; set; }

        public string Refresh_Token { get; set; }

        public string ClientId { get; set; }

        public string UserName { get; set; }

        public string Message { get; set; }

        public DateTime Issued { get; set; }

        public DateTime Expires { get; set; }


    }
}
