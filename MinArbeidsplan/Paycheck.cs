﻿namespace MinArbeidsplan
{
    public class Paycheck
    {

     public int CompanyId { get; set; }

     public string EmploymentNo { get; set; }

     public string Periode { get; set; }

     public string Key1 { get; set; }

     public string Description { get; set; }

     public string Doc { get; set; }

     public bool IsSent { get; set; }

    }
}
