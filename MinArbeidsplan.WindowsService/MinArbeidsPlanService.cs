﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace MinArbeidsplan.WindowsService
{
    public partial class MinArbeidsPlanService : ServiceBase
    {
        public MinArbeidsPlanService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CheckLonnslippsIntervalled();
        }

        private void CheckLonnslippsIntervalled()
        {
            CheckLonnslipps();
            Task.Delay(TimeSpan.FromMinutes(60*8)).ContinueWith(task => CheckLonnslippsIntervalled());
        }

        private void CheckLonnslipps()
        {
            try
            {
                string executable = typeof(MinArbeidsplan.Paycheck).Assembly.Location;
                EventLog.WriteEntry(
                    "Starting retrieval of lonnsslipps through MinArbeidsPlanService Windows Service..");

                Process.Start(executable);
                EventLog.WriteEntry(
                    "Starting retrieval of lonnsslipps through MinArbeidsPlanService Windows Service..");
            }
            catch (Exception err)
            {
                Console.WriteLine(@"There was an exception in MinArbeidsplanService: " + err.Message);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
